CREATE TABLE `ngaanu`.`short_url` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `original_url` TEXT NOT NULL,
    `short_url` VARCHAR(255) NOT NULL,
    `unique_id` VARCHAR(255) NOT NULL,
    `custom_name` VARCHAR(255) NULL DEFAULT NULL,
    `is_password_protected` TINYINT(1) DEFAULT 0,
    `user_ip` VARCHAR(25) NULL DEFAULT NULL,
    `expired_at` DATE NULL DEFAULT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
);

CREATE INDEX idx_unique_id
ON `ngaanu`.`short_url` (`unique_id`);

CREATE INDEX idx_custom_name
ON `ngaanu`.`short_url` (`custom_name`);