package main

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"github.com/urfave/negroni"
	"gitlab.com/mydisha/ngaanu/pkg/handler"
	"net/http"
)

func main() {
	router := httprouter.New()
	setRoute(router)

	n := negroni.Classic()
	n.UseHandler(router)

	port := "8083"

	fmt.Println(fmt.Sprintf("Starting ngaa.nu on port %s", port))
	http.ListenAndServe(fmt.Sprintf(":%s", port), n)
}

func setRoute(router *httprouter.Router) {
	router.POST("/v1/shortner/create", handler.CreateShortUrlHandler)
}
