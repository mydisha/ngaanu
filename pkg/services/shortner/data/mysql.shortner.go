package data

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/mydisha/ngaanu/pkg/common/database"
)

type MysqlShortnerData struct {
}

type MysqlShortnerStatement struct {
	CreateQuery *sqlx.Stmt
}

const (
	CreateShortnerQuery = "INSERT INTO short_url (original_url, short_url, unique_id, custom_name, is_password_protected, user_ip, expired_at, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
)

var shortnerStmt MysqlShortnerStatement

func init() {
	shortnerStmt.CreateQuery = database.Prepare(database.GetDatabase(), CreateShortnerQuery)
}

func (m MysqlShortnerData) Create(shortnerPayload ShortnerPayload) (uint64, error) {
	createdShortUrl, err := shortnerStmt.CreateQuery.Exec(
		shortnerPayload.OriginalUrl,
		shortnerPayload.ShortUrl,
		shortnerPayload.UniqueId,
		shortnerPayload.CustomName,
		shortnerPayload.IsPasswordProtected,
		shortnerPayload.UserIp,
		shortnerPayload.ExpiredAt,
		shortnerPayload.CreatedAt,
	)

	if err != nil {
		return 0, err
	}

	lastInsertId, err := createdShortUrl.LastInsertId()

	if err != nil {
		return 0, err
	}

	return uint64(lastInsertId), nil
}
