package data

import "time"

type ShortnerPayload struct {
	Id                  uint64     `json:"id" db:"id"`
	OriginalUrl         string     `json:"original_url" db:"original_url"`
	ShortUrl            string     `json:"short_url" db:"short_url"`
	UniqueId            string     `json:"unique_id" db:"unique_id"`
	CustomName          string     `json:"custom_name" dB:"custom_name"`
	IsPasswordProtected bool       `json:"is_password_protected" db:"is_password_protected"`
	UserIp              string     `json:"user_ip" db:"user_id"`
	ExpiredAt           *time.Time `json:"expired_at" db:"expired_at"`
	CreatedAt           time.Time  `json:"created_at" db:"created_at"`
}

type ShortnerInterface interface {
	Create(shortnerPayload ShortnerPayload) (uint64, error)
}
