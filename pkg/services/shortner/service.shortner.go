package shortner

import (
	"gitlab.com/mydisha/ngaanu/pkg/services/shortner/data"
	"time"
)

type ShortnerPayload struct {
	OriginalUrl string `json:"original_url"`
}

type ShortnerService struct {
	shortner data.ShortnerInterface
}

func NewShortnerService() ShortnerService {
	return ShortnerService{
		shortner: data.MysqlShortnerData{},
	}
}

func (s ShortnerService) CreateShortner(payload ShortnerPayload) (uint64, error) {

	shortnerPayload := data.ShortnerPayload{
		OriginalUrl: payload.OriginalUrl,
		CreatedAt:   time.Now(),
	}

	shortUrl, err := s.shortner.Create(shortnerPayload)

	if err != nil {
		return 0, err
	}

	return shortUrl, nil
}
