package handler

import (
	"encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/mydisha/ngaanu/pkg/services/shortner"
	"io/ioutil"
	"log"
	"net/http"
)

func CreateShortUrlHandler(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	b, err := ioutil.ReadAll(request.Body)

	if err != nil {
		fmt.Print(fmt.Sprintf("Error %v", err.Error()))
	}

	var shortnerPayload shortner.ShortnerPayload

	if err := json.Unmarshal(b, &shortnerPayload); err != nil {
		fmt.Print(fmt.Sprintf(err.Error()))
	}

	fmt.Print(fmt.Sprintf("Original url %s", shortnerPayload.OriginalUrl))

	//var redisClient = redis.GetRedisClient()

	shortnerService := shortner.NewShortnerService()
	created, err := shortnerService.CreateShortner(shortnerPayload)

	if err != nil {
		log.Print(err.Error())
	}

	log.Print(created)
}
