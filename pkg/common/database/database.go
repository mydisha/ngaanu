package database

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"log"
)

type Config struct {
	DatabaseConn string `json:"database_conn"`
}

var dbConn *sqlx.DB

func init() {
	databaseConnection := "mydisha:mydisha@tcp(127.0.0.1:3306)/ngaanu?parseTime=true"
	db, err := sqlx.Open("mysql", databaseConnection)
	if err != nil {
		panic("Cannot connect to " + databaseConnection)
	}
	dbConn = db
}

func GetDatabase() *sqlx.DB {
	return dbConn
}

func Prepare(db *sqlx.DB, query string) *sqlx.Stmt {
	stmt, err := db.Preparex(query)
	if err != nil {
		log.Print(err.Error())
	}
	return stmt
}
